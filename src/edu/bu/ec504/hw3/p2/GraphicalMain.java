package edu.bu.ec504.hw3.p2;

import static javax.swing.SwingUtilities.invokeLater;

import edu.bu.ec504.hw3.p2.Canvas.CanvasPoint;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * A graphical version of the main class, which may be useful for debugging.
 * (FYI: modeled after chatGPT4 response to "Please give me a sample program that draws a line on a 2d java window.")
 */
public class GraphicalMain extends JFrame {
  final int prefX = 10;
  final int prefY = 10;
  final int xScale = 30; // scale all X coordinates on the Canvas by this amount
  final int yScale = 30; // scale all X coordinates on the Canvas by this amount
  final double pointRad = 1d/10d; // radius of a point
  final double coverRad = 1d;    // radius of a covering circle
  final Color pointCol = new Color(255,0,0,255); // color for a point
  final Color coverCol = new Color(0,0,255,50); // color for a covering circle


  final Main.CoverResult toDraw; // the points/circles to draw


  public GraphicalMain() {
    this.setTitle("HW3P2");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.add(new DrawCovering());
    this.pack();
    this.setLocationRelativeTo(null);
    this.setVisible(true);

    // create the canvas and compute centers
    toDraw = Main.testCovering(prefX, prefY);
  }
  class DrawCovering extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);

      // draw canvas points
      g.setColor(pointCol);
      for (CanvasPoint pnt: toDraw.canvas) {
        g.fillOval(
            (int) ((pnt.x-pointRad/2)*xScale),
            (int) ((pnt.y-pointRad/2)*yScale),
            (int) (pointRad*xScale),
            (int) (pointRad*yScale)
        );
      }

      // draw covering circles
      g.setColor(coverCol);
      for (CanvasPoint cnt: toDraw.centers) {

        System.out.println("Drawing: "+cnt);
        g.fillOval(
            (int) ((cnt.x-coverRad/2)*xScale),
            (int) ((cnt.y-coverRad/2)*yScale),
            (int) (coverRad*xScale),
            (int) (coverRad*yScale));
      }

    }

    @Override
    public Dimension getPreferredSize() {
      return new Dimension(prefX*xScale,prefY*yScale);
    }
  }

  public static void main(String[] args) {
    invokeLater(GraphicalMain::new);
  }
}
